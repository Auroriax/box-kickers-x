///scr_keybindinit

keyb[1,KEY_UP] = vk_up
keyb[1,KEY_LEFT] = vk_left
keyb[1,KEY_DOWN] = vk_down
keyb[1,KEY_RIGHT] = vk_right

if os_browser != browser_not_a_browser
{
keyb[1,KEY_KICK] = vk_shift
keyb[1,KEY_KICKALT] = vk_alt
}
else
{
keyb[1,KEY_KICK] = vk_rcontrol
keyb[1,KEY_KICKALT] = vk_rshift
}

keyb[2,KEY_UP] = ord('W')
keyb[2,KEY_LEFT] = ord('A')
keyb[2,KEY_DOWN] = ord('S')
keyb[2,KEY_RIGHT] = ord('D')
keyb[2,KEY_KICK] = vk_space
keyb[2,KEY_KICKALT] = ord('E')

keyb[3,KEY_UP] = ord('I')
keyb[3,KEY_LEFT] = ord('J')
keyb[3,KEY_DOWN] = ord('K')
keyb[3,KEY_RIGHT] = ord('L')
keyb[3,KEY_KICK] = ord('U')
keyb[3,KEY_KICKALT] = ord('O')

keyb[4,KEY_UP] = vk_numpad5
keyb[4,KEY_LEFT] = vk_numpad1
keyb[4,KEY_DOWN] = vk_numpad2
keyb[4,KEY_RIGHT] = vk_numpad3
keyb[4,KEY_KICK] = vk_numpad0
keyb[4,KEY_KICKALT] = vk_numpad6
