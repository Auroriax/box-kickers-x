//Checks if a level applies to the rules, and if so, returns true.
level = argument0
//Filtering eventual useless chars
check = level_adjust(level)

if string_count("q",string_copy(check,1,450)) == 0 {return(0)}
if string_count("w",string_copy(check,1,450)) == 0 {return(0)}
if string_count("e",string_copy(check,1,450)) == 0 {return(0)}
if string_count("r",string_copy(check,1,450)) == 0 {return(0)}
if string_length(check) <= 449 {return(0)}
return(1)
