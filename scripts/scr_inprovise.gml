///(WARNING! This is a very heavy and advanced script that creates a level from a txt file. The level to be loaded, is the only argument.)

//Loading level. 25 chars wide, 18 rows height.
level = argument0
//Filtering eventual useless chars
global.levelf = level_adjust(level)

//Check if the stage supplies to our absolute minimum requirements.
if string_count("q",string_copy(global.levelf,1,450)) == 0 {show_error("ERROR: No respawn point for Player 1. Aborting level loading.",0) room_goto(LvlSelect); exit}
if string_count("w",string_copy(global.levelf,1,450)) == 0 {show_error("ERROR: No respawn point for Player 2. Aborting level loading.",0) room_goto(LvlSelect); exit}
if string_count("e",string_copy(global.levelf,1,450)) == 0 {show_error("ERROR: No respawn point for Player 3. Aborting level loading.",0) room_goto(LvlSelect); exit}
if string_count("r",string_copy(global.levelf,1,450)) == 0 {show_error("ERROR: No respawn point for Player 4. Aborting level loading.",0) room_goto(LvlSelect); exit}
//if string_count("q",string_copy(global.levelf,1,450)) >= 2 {show_error("ERROR: Multiple respawn points for Player 1. Aborting level loading.",0) room_goto(LvlSelect); exit}
//if string_count("w",string_copy(global.levelf,1,450)) >= 2 {show_error("ERROR: Multiple respawn points for Player 2. Aborting level loading.",0) room_goto(LvlSelect); exit}
if string_length(global.levelf) <= 449 {show_error("ERROR: Not enough chars to build a level from. Make sure there are 450 chars available. Aborting level loading.",0) room_goto(LvlSelect); exit}

//Actual placement of stuff
for(j=0;j!=18;j+=1)
 {for(i=0;i!=25;i+=1)
  {
  k = (i+(j*25)+1)
  switch (string_char_at(global.levelf,k))
   {case ".": /*Use dots to indicate empty spaces. It speed up the loading of the level!*/ break;
    case "s": instance_create(i*32+16,j*32+16,o_box); break;
    case "m": instance_create(i*32+16,j*32+16,o_box24); break;
    case "l": instance_create(i*32+16,j*32+16,o_box32); break;
    case "i": instance_create(i*32+16,j*32+16,o_iceblock); break;
    case "1": instance_create(i*32+16,j*32+16,o_p1); break;
    case "2": instance_create(i*32+16,j*32+16,o_p2); break;
    case "3": instance_create(i*32+16,j*32+16,o_p3); break;
    case "4": instance_create(i*32+16,j*32+16,o_p4); break;
    case "q": instance_create(i*32+16,j*32+16,o_p1spawner) break;
    case "w": instance_create(i*32+16,j*32+16,o_p2spawner) break;
    case "e": instance_create(i*32+16,j*32+16,o_p3spawner) break;
    case "r": instance_create(i*32+16,j*32+16,o_p4spawner) break;
    case "h": instance_create(i*32+16,j*32+16,o_digitalsmall); break;
    case "b": instance_create(i*32+16,j*32+16,o_digitalbreak) break;
    case "o": instance_create(i*32+16,j*32+16,o_bomb) break;
    case "p": instance_create(i*32+16,j*32+16,o_crystal) break;
    case "/": instance_create(i*32+16,j*32+16,o_slope) break;
    case "\": instance_create(i*32+16,j*32+16,o_slope2) break;
    case "%": instance_create(i*32+16,j*32+16,o_slope3) break;
    case "a": instance_create(i*32+16,j*32+16,o_slope4) break;
    case "[": instance_create(i*32+16,j*32+16,o_escr) break;
    case "?": instance_create(i*32+16,j*32+16,o_escup) break;
    case "]": instance_create(i*32+16,j*32+16,o_escleft) break;
    case "&": instance_create(i*32+16,j*32+16,o_escdown) break;
  }
  }
 }
 
 //As a safeguard for wrapping levels, some extra blocks will spawn
 
//The level name, if included, will be displayed in the windows' title bar.
if room == Interperter
room_caption = global.setname+" - Box Kickers X"

background_color    = real(string_copy(global.levelf,string_pos("(",global.levelf)+1,string_pos(")",global.levelf)-string_pos("(",global.levelf)-1))
global.setcolor = real(string_copy(global.levelf,string_pos("{",global.levelf)+1,string_pos("}",global.levelf)-string_pos("{",global.levelf)-1))
background_blend[0] = global.setcolor
