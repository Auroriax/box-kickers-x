///audio_start(soundid,loops?)
///Deals with both the old and new sound system.
///It also checks some other things, like audio settings, and not restarting already playing sounds.

//If sfx are turned off, and attempting to play a non-looped audio file, cancel script
if (global.sfx && !argument1) {exit;}

if audio_system() == audio_new_system
{
 if ((!argument1) || (!audio_is_playing(argument0) && argument1))
 audio_play_sound(argument0,0,argument1)
}
else //Legacy system
{
 if argument1
 {
  if !sound_isplaying(argument0) {sound_loop(argument0) }
 }
 else
 {sound_play(argument0) }
}
