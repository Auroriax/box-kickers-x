//argument[0] = left, [1] = top; [2] = scale; [3] = level to be drawn
var i, j, s;

for(j=0;j!=18;j+=1)
 {for(i=0;i!=25;i+=1)
  { 
  s = getspritebychar(string_copy(argument[3],i+(j*25)+1,1))
  
  if s != s_escr8new
  {
  if (s == s_digitalsmall || s == s_digitalbreak || s == s_slope || s == s_slope2 || s == s_slope3 || s == s_slope4)
  draw_sprite_ext(s,0,i*(32*argument[2])+16+argument[0],j*(32*argument[2])+16+argument[1],argument[2],argument[2],0,global.setcolor,1)
  else
  draw_sprite_ext(s,0,i*(32*argument[2])+16+argument[0],j*(32*argument[2])+16+argument[1],argument[2],argument[2],0,-1,1)
  }
  else
  switch (string_copy(argument[3],i+(j*25)+1,1))
  {
  case "[": draw_sprite_ext(s,0,i*(32*argument[2])+16+argument[0],j*(32*argument[2])+16+argument[1],argument[2],argument[2],0,global.setcolor,1); break;
  case "?": draw_sprite_ext(s,0,i*(32*argument[2])+16+argument[0],j*(32*argument[2])+16+argument[1],argument[2],argument[2],90,global.setcolor,1); break;
  case "]": draw_sprite_ext(s,0,i*(32*argument[2])+16+argument[0],j*(32*argument[2])+16+argument[1],argument[2],argument[2],180,global.setcolor,1); break;
  case "&": draw_sprite_ext(s,0,i*(32*argument[2])+16+argument[0],j*(32*argument[2])+16+argument[1],argument[2],argument[2],270,global.setcolor,1); break;
  }
  
    //if keyboard_check(vk_space)
    //draw_text(i*32+16,j*32,string_copy(argument[3],i+(j*25)+1,1))
  }}
