//This code will start a kick of the player. alarm[1] will stop the kicking,
//and alarm[2] will set kickable back to true, so the player can kick again.

if instance_exists(o_control)
if o_control.timeleft > 0
{global.kickamount[playernr] += 1}

kickable = false ; kicking = true; alarm[1] = 20
sprite_index = kick_sprite

//If the direction has been set by arguments, kick towards that direction.
if argument0 != 0
{motion_set(argument0,8); exit}
//Elseway, we'll figure it out ourselves.

//Here, we set in which direction the player is going to kick.
if keyboard_check(right) && keyboard_check(down)  {motion_set(315,8);} else
if keyboard_check(down)  && keyboard_check(left)  {motion_set(225,8);} else
if keyboard_check(left)  && keyboard_check(up)    {motion_set(135,8);} else
if keyboard_check(up)    && keyboard_check(right) {motion_set(45 ,8);} else
if keyboard_check(down)  {motion_set(270,8);} else
if keyboard_check(left)  {motion_set(180,8);} else
if keyboard_check(up)    {motion_set(90,8) ;} else
if keyboard_check(right) {motion_set(360,8)} else
//If the player doesn't presses any keys, we try to
//derive the intended direction from the current subimage.
{
switch image_index
{case 0: motion_set(0,8);   break;
 case 1: motion_set(270,8); break;
 case 2: motion_set(180,8); break;
 case 3: motion_set(90,8);  break;} 
}

//Sillyness
if global.fartmode == true
{
 switch irandom_range(2,3)
 {
 case 2: audio_start(sou_fart2, false); break;
 case 3: audio_start(sou_fart3, false); break;
 }
 effect_create_above(ef_smokeup,x,y,0,make_color_rgb(143,100,66))
}
