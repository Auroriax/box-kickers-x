//Gives a random, often useless piece of info to the player. Hey, it worked in Minecraft!
//Argument -1 is true randomness, any other number will return that specific tip.
var rice;
if argument0 = -1
rice = irandom(49)
else
rice = argument0

switch (rice)
{
case 0: return("Think ahead when kicking a box. What chain reaction will it result in?");
case 1: return("And, do you like the game? All feedback is welcome @AmazingcookieNL");
case 2: return("From my gameplay idea of attacking indirectly, grew Box Kickers.");
case 3: return("Bombs can backfire when bounced back to you with a box.");
case 4: return("When boxes bounce, their speed is added up, then divided by two.");
//Indie Game promotion
case 5: return("In Micro Massive, press F11 to get a flashy pink party hat!");
case 6: return("Anodyne is awesome! It's a surreal adventure. Your main weapon is a broom.");
case 7: return("VVVVVV is amazing! You can't jump, just flip gravity around. It's hard!");
case 8: return("BIT.TRIP is super! Six games, all with unique gameplay. FLUX is my favorite.");
case 9: return("Knytt Underground is fantastic! No hurries, just explore. Collect, transform and more!");
case 10: return("Two Tribes is incredible! It's the Dutch indie studio behind Toki Tori, RUSH, and more.");
case 11: return("World of Goo is astonishing! Great puzzle game with an amazing atmosphere and music.");
case 12: return("Also play Cave Story! ...what? You've already played it? Go play it again. Now.");
case 13: return("Kairo is epic. Explore and discover. Give life to the world around you.");
case 14: return("Really Big Sky is really good! Fly through space and blow things up.");
case 15: return("Sequence is extraordinary. A rhythm game like no other. Completely voice acted!");
case 16: return("Shovel Knight is marvelous! But then again, you probably already knew that.");
case 17: return("Thomas was Alone is a really good puzzle platformer with awesome music. Rectangles with personality!");
case 18: return("Teslagrad is wonderful. Magnetic puzzling in this metroidvania. A story told without words.");
case 19: return("Also play Hero Core! Black and white, but good and right. Battle Tetron and end his reign!");
case 20: return("Looking for hard puzzle games? Play Tetrobot and Co.! A very polished and clever game.");
//Other useless info
case 21: return("My other games are Micro Massive, Subarashi, Mathventure, Picross Pushers, and more!");
case 22: return("Don't forget to brush your teeth at least twice a day! At least.");
case 23: return("Pancakes! Who doesn't love pancakes?");
case 24: return("Curtains are expensive.");
case 25: return("If you like this game, you might also like Box Kickers! (Recursion, I love it :D )");
case 26: return("Play the game on 19 Juli for a special suprise!");
//Level Editor tips
case 27: return("In the level editor, have you tried removing the arena walls?");
case 28: return("In the level editor, make sure you don't create shallow spaces. Boxes have a hard time going through them.");
case 29: return("In the level editor, don't place boxes in corners! It's too hard for players to get 'em out.");
case 30: return("In the level editor, just play around! Everything is possible, just place spawnpoints.");
case 31: return("In the level editor, you can adjust the background color. Freedom ahoy!");
case 32: return("In the level editor, you can use all assets used in the 'Classic' levels, exept the big boxes seen in Mega Box Land.");
case 33: return("In the level editor, don't place boxes to close too each other, or place too many.");
//More wierdness
case 34: return("This game is the sequel of Box Kickers, which appeared in 2013!");
case 35: return("While this game is technically a sequel, it reuses lots of code and assets from its predecessor.");
case 36: return("Boxes turn into the color of the player who kicked it. Try not to hit yourself.");
//Quotes
case 37: return("You have to learn the rules of the game. And then you have to play better than anyone else. -Albert Einstein");
case 38: return("Sometimes by losing a battle you find a new way to win the war. -Donald Trump");
case 39: return("You may have to fight a battle more than once to win it. -Margaret Thatcher");
//More gameplay tips
case 40: return("Why not use a controller to play? P1 uses controller 1, P2 controller 2.");
case 41: return("Have you already tried four player multiplayer? It can get mighty hectic!");
case 42: return("Kick a box to the edge of the screen and it'll appear on the other side! You can do that, too.");
case 43: return("There is no gameplay difference between the different characters, except starting locations.");
case 44: return("Box Kickers is lightly inspired by multiplayer classics as Bomberman and Smash Brothers. Thanks!");
case 45: return("Special thanks for GameJolt and its community.");
case 46: return("Did you know you are awesome?");
case 47: return("If you thought the only good game about boxes was Sokoban, then you might want to revise that opinion!");
case 48: return("Thanks for playing!");
case 49: return("Check out my website: amcookie.weebly.com");
}
