//Removes all useless chars from the level string.
level = argument0
level1 = string_lower(level)
level2 = string_replace_all(level1,chr(13),"")
level3 = string_replace_all(level2,chr(10),"")
level4 = string_replace_all(level3,"#","")
level5 = string_replace_all(level4,"message","")
return(string_lower(level5))
