///audio_stop(soundid [use -1 to stop all sounds])
///Deals with both the old and new sound system.

if audio_system() == audio_new_system
{
 if argument0 != -1
  {audio_stop_sound(argument0)}
 else
  {audio_stop_all()}
}
else //Legacy system
{
 if argument0 != -1
  sound_stop(argument0)
 else
  sound_stop_all()
}
