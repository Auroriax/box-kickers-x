//Accepts one argument: the heavyness of the shake.
//Found at http://www.game-maker.nl/forums/topic,39624.0/prev_next,next

if instance_exists(o_control)
{
if view_xview[0]<=0 then view_xview[0]+=random(argument0)
else if view_xview[0]>0 then view_xview[0]-=random(argument0)
if view_yview[0]<=0 then view_yview[0]+=random(argument0)
else if view_yview[0]>0 then view_yview[0]-=random(argument0)
o_control.alarm[11] = 2
}
