if string_count("q",string_copy(o_leveleditor.level,1,450)) == 0 {show_message("ERROR: No respawn point for Player 1. Please place one."); exit}
if string_count("w",string_copy(o_leveleditor.level,1,450)) == 0 {show_message("ERROR: No respawn point for Player 2. Please place one."); exit}
if string_count("e",string_copy(o_leveleditor.level,1,450)) == 0 {show_message("ERROR: No respawn point for Player 3. Please place one."); exit}
if string_count("r",string_copy(o_leveleditor.level,1,450)) == 0 {show_message("ERROR: No respawn point for Player 4. Please place one."); exit}
if string_length(o_leveleditor.level) <= 449 {show_message("ERROR: Not enough chars to build a level from. Aborting level loading."); exit}
{
global.setlevel = o_leveleditor.level+"("+string(background_color)+"){"+string(global.setcolor)+"}"
transition_kind = 17
transition_steps = 15
room_goto(TestRoom);
}
