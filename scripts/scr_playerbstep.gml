//This will start a kick when the corresponding button is pressed
if (keyboard_check(keyb[playernr,KEY_KICK]) == true || keyboard_check(keyb[playernr,KEY_KICKALT]) == true) && kicking == false && kickable == true
scr_startkick(0)

//This script handles walking around while not kicking
if kicking == 0
{
 if place_meeting(x,y,o_box)
 {
 if keyboard_check(up)   && place_free(x,y-4) && place_meeting(x,y-4,o_box) {y -= 4; global.pxwalked[playernr]+= 4; if y <= 0   {y = room_height; if place_meeting(x,y,o_box) > 0 { y=yprevious; }} }
 if keyboard_check(left) && place_free(x-4,y) && place_meeting(x-4,y,o_box) {x -= 4; global.pxwalked[playernr]+= 4; if x <= 0   {x = room_width; if place_meeting(x,y,o_box) > 0 { x=xprevious; }} }
 if keyboard_check(down) && place_free(x,y+4) && place_meeting(x,y+4,o_box) {y += 4; global.pxwalked[playernr]+= 4; if y >= room_height {y=0; if place_meeting(x,y,o_box) > 0 { y=yprevious; }}     }
 if keyboard_check(right)&& place_free(x+4,y) && place_meeting(x+4,y,o_box) {x += 4; global.pxwalked[playernr]+= 4; if x >= room_width {x = 0; if place_meeting(x,y,o_box) > 0 { x=xprevious; }}   }
 }
  if keyboard_check(up)   && place_free(x,y-4) && !place_meeting(x,y-4,o_box) {y -= 4; global.pxwalked[playernr]+= 4; if y <= 0   {y = room_height; if place_meeting(x,y,o_box) > 0 { y=yprevious; }} }
  if keyboard_check(left) && place_free(x-4,y) && !place_meeting(x-4,y,o_box) {x -= 4; global.pxwalked[playernr]+= 4; if x <= 0   {x = room_width; if place_meeting(x,y,o_box) > 0 { x=xprevious; }} }
  if keyboard_check(down) && place_free(x,y+4) && !place_meeting(x,y+4,o_box) {y += 4; global.pxwalked[playernr]+= 4; if y >= room_height {y=0; if place_meeting(x,y,o_box) > 0 { y=yprevious; }}     }
  if keyboard_check(right)&& place_free(x+4,y) && !place_meeting(x+4,y,o_box) {x += 4; global.pxwalked[playernr]+= 4; if x >= room_width {x = 0; if place_meeting(x,y,o_box) > 0 { x=xprevious; }}   }
  }

//Code that controls which sprite is shown/in which direction the player is looking

if keyboard_check(right) && keyboard_check(down)  {image_index = 0; exit }
if keyboard_check(down)  && keyboard_check(left)  {image_index = 1; exit }
if keyboard_check(left)  && keyboard_check(up)    {image_index = 2; exit }
if keyboard_check(up)    && keyboard_check(right) {image_index = 3; exit }

if keyboard_check(down)  {image_index = 1; exit }
if keyboard_check(left)  {image_index = 2; exit }
if keyboard_check(up)    {image_index = 3; exit }
if keyboard_check(right) {image_index = 0; exit }
