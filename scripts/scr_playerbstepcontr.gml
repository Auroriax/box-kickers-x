if global.joystick == false {exit;}

var p;
p = argument[0]
if kicking == 0 && xprevious == x && yprevious == y
{
 if place_meeting(x,y,o_box)
 {
  if (joystick_direction(p) = vk_numpad7 || joystick_direction(p) = vk_numpad8 || joystick_direction(p) = vk_numpad9) && place_free(x,y-4) && place_meeting(x,y-4,o_box) 
  {y -= 4; global.pxwalked[playernr]+= 4; if y <= 0   {y = room_height; if place_meeting(x,y,o_box) > 0 { y=yprevious; }} }
  if (joystick_direction(p) = vk_numpad1 || joystick_direction(p) = vk_numpad4 || joystick_direction(p) = vk_numpad7) && place_free(x-4,y) && place_meeting(x-4,y,o_box) 
  {x -= 4; global.pxwalked[playernr]+= 4; if x <= 0   {x = room_width; if place_meeting(x,y,o_box) > 0 { x=xprevious; }} }
  if (joystick_direction(p) = vk_numpad1 || joystick_direction(p) = vk_numpad2 || joystick_direction(p) = vk_numpad3) && place_free(x,y+4) && place_meeting(x,y+4,o_box) 
  {y += 4; global.pxwalked[playernr]+= 4; if y >= room_height {y = 0; if place_meeting(x,y,o_box) > 0 { y=yprevious; }} }
  if (joystick_direction(p) = vk_numpad3 || joystick_direction(p) = vk_numpad6 || joystick_direction(p) = vk_numpad9) && place_free(x+4,y) && place_meeting(x+4,y,o_box) 
  {x += 4; global.pxwalked[playernr]+= 4; if x >= room_width {x = 0; if place_meeting(x,y,o_box) > 0 { x=xprevious; }} }
 }
 if (joystick_direction(p) = vk_numpad7 || joystick_direction(p) = vk_numpad8 || joystick_direction(p) = vk_numpad9) && place_free(x,y-4) && !place_meeting(x,y-4,o_box) 
 {y -= 4; global.pxwalked[playernr]+= 4; if y <= 0   {y = room_height; if place_meeting(x,y,o_box) > 0 { y=yprevious; }} }
 if (joystick_direction(p) = vk_numpad1 || joystick_direction(p) = vk_numpad4 || joystick_direction(p) = vk_numpad7) && place_free(x-4,y) && !place_meeting(x-4,y,o_box) 
 {x -= 4; global.pxwalked[playernr]+= 4; if x <= 0   {x = room_width; if place_meeting(x,y,o_box) > 0 { x=xprevious; }} }
 if (joystick_direction(p) = vk_numpad1 || joystick_direction(p) = vk_numpad2 || joystick_direction(p) = vk_numpad3) && place_free(x,y+4) && !place_meeting(x,y+4,o_box) 
 {y += 4; global.pxwalked[playernr]+= 4; if y >= room_height {y = 0; if place_meeting(x,y,o_box) > 0 { y=yprevious; }} }
 if (joystick_direction(p) = vk_numpad3 || joystick_direction(p) = vk_numpad6 || joystick_direction(p) = vk_numpad9) && place_free(x+4,y) && !place_meeting(x+4,y,o_box) 
 {x += 4; global.pxwalked[playernr]+= 4; if x >= room_width {x = 0; if place_meeting(x,y,o_box) > 0 { x=xprevious; }} }
}

//Code that controls which sprite is shown/in which direction the player is looking
if joystick_direction(p) = vk_numpad3 || joystick_direction(p) = vk_numpad6 {image_index = 0; exit }
if joystick_direction(p) = vk_numpad1 || joystick_direction(p) = vk_numpad2 {image_index = 1; exit }
if joystick_direction(p) = vk_numpad7 || joystick_direction(p) = vk_numpad4 {image_index = 2; exit }
if joystick_direction(p) = vk_numpad9 || joystick_direction(p) = vk_numpad8 {image_index = 3; exit }
